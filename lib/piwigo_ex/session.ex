defmodule PiwigoEx.Session do
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint
  alias PiwigoEx.Helpers.ErrorHandler

  @moduledoc false

  @doc """
  Login with a single site instance of Piwigo_ex with only 1 user with environment variables: PIWIGO_HOST, PIWIGO_USER,
  PIWIGO_PASS.  Opts keyword list can be used to override any of those env variables:

  Options:
  :user overrides PIWIGO_USER
  :pass overrides PIWIGO_PASS
  :host overrides PIWIGO_HOST

  """
  def login(opts \\ []) do
    login(
      Keyword.get(opts, :user, user()),
      Keyword.get(opts, :pass, pass()),
      Keyword.get(opts, :host, Config.host())
    )
  end

  # Login and get a pwg_id if the username and password are correct
  defp login(username, password, host)
       when is_binary(username) and is_binary(password) and is_binary(host) do
    with {:ok, %Finch.Response{body: body, headers: headers, status: 200}} <-
           Config.session_api().login_request(username, password, host),
         {:ok, json} <- Jason.decode(body),
         {:ok, "ok"} <- Map.fetch(json, "stat") do
      cookie_value =
        Enum.find_value(
          headers,
          fn {header, value} ->
            if header == "set-cookie" && String.starts_with?(value, "pwg_id="), do: value
          end
        )

      # The first part of the cookie is the ID, but we don't want the pwg_id= prefix so take that out too
      token =
        cookie_value
        |> String.split(";")
        |> List.first()

      {:ok, token}
    else
      err -> ErrorHandler.handle_error(err)
    end
  end

  @doc """
  Log the current user out.  Currently the only option to pass in is the host: [host: "host"]
  """
  def logout(opts \\ []) do
    logout_host(Keyword.get(opts, :host, Config.host()))
  end

  # Log the current user out
  defp logout_host(host) when is_binary(host) do
    function = fn -> Config.session_api().logout_request(host) end

    case CallEndpoint.handle_result(function) do
      {:success, _json} -> :ok
      err -> ErrorHandler.handle_error(err)
    end
  end

  defp user() do
    case System.fetch_env("PIWIGO_USER") do
      {:ok, user} -> user
      _ -> nil
    end
  end

  defp pass() do
    case System.fetch_env("PIWIGO_PASS") do
      {:ok, pass} -> pass
      _ -> nil
    end
  end
end
