defmodule PiwigoEx.Helpers.CallEndpoint do
  alias PiwigoEx.Config

  @moduledoc """
  A helper to call Piwigo and handle results
  """

  @doc """
  Handle results from Piwigo calls to determine if it fails or succeeds
  """
  def handle_result(function) do
    with {:ok, %Finch.Response{body: body, status: 200}} <- function.(),
         {:ok, json} <- Jason.decode(body),
         {:ok, "ok"} <- Map.fetch(json, "stat") do
      # This sends back success instead of :ok because :ok is also used for 404, etc
      {:success, json}
    end
  end

  @doc """
  Make a simple GET request to Piwigo
  """
  def get(url, opts \\ []) do
    Finch.build(:get, url, opts)
    |> Finch.request(PiwigoFinch)
  end

  @doc """
  Make a post request to Piwigo and set the content type to application/x-www-form-urlencoded
  """
  def post(host, post_data, opts \\ []) do
    headers = [{"content-type", "application/x-www-form-urlencoded"} | opts]

    Finch.build(:post, Config.url(host), headers, post_data)
    |> Finch.request(PiwigoFinch)
  end
end
