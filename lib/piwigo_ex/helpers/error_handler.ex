defmodule PiwigoEx.Helpers.ErrorHandler do
  @moduledoc false

  # There is no error for missing tag id
  def handle_error({
        :ok,
        %Finch.Response{
          body: "{\"stat\":\"fail\",\"err\":404,\"message\":\"image_id not found\"}",
          status: 404
        }
      }) do
    {:error, :invalid_image_id}
  end

  def handle_error({:ok, %Finch.Response{status: 404}}) do
    {:error, :invalid_url}
  end

  def handle_error({:ok, %Finch.Response{status: 401}}) do
    {:error, :unauthorized}
  end

  def handle_error({:ok, "fail"}) do
    {:error, :invalid_username_password}
  end

  def handle_error(err) do
    {:error, err}
  end
end
