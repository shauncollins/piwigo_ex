defmodule PiwigoEx.PiwigoImpl.InMemoryTags do
  @behaviour PiwigoEx.PiwigoImpl.TagsInterface

  @moduledoc """
  Fakes tag requests to Piwigo server
  """

  def tags_request("bad_host") do
    {:ok,
     %Finch.Response{
       status: 404
     }}
  end

  def tags_request("one_tag") do
    {
      :ok,
      %Finch.Response{
        body:
          "{\"stat\":\"ok\",\"result\":{\"tags\":[{\"id\":1,\"name\":\"tags\",\"url_name\":\"tags\",\"lastmodified\":\"2021-02-28 17:09:12\",\"counter\":206,\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/tags\\/1-tags\"}]}}",
        headers: [],
        status: 200
      }
    }
  end

  def admin_tags_request("bad_host", _) do
    {:ok,
     %Finch.Response{
       status: 404
     }}
  end

  def admin_tags_request("good_host", "bad_token") do
    {
      :ok,
      %Finch.Response{
        body: "{\"stat\":\"fail\",\"err\":401,\"message\":\"Access denied\"}",
        headers: [],
        status: 401
      }
    }
  end

  def admin_tags_request("good_host", "good_token") do
    {
      :ok,
      %Finch.Response{
        body:
          "{\"stat\":\"ok\",\"result\":{\"tags\":[{\"id\":1,\"name\":\"tags\",\"url_name\":\"tags\",\"lastmodified\":\"2021-02-28 17:09:12\",\"counter\":206,\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/tags\\/1-tags\"}]}}",
        headers: [],
        status: 200
      }
    }
  end

  def images_request("bad_host", _, _, _) do
    {:ok,
     %Finch.Response{
       status: 404
     }}
  end

  def images_request("good_host", _, _, %PiwigoEx.PageRequest{page: 1}) do
    {
      :ok,
      %Finch.Response{
        body:
          "{\"stat\":\"ok\",\"result\":{\"paging\":{\"page\":1,\"per_page\":100,\"count\":100,\"total_count\":206},\"images\":[{\"rank\":0,\"id\":365,\"width\":4912,\"height\":7360,\"hit\":0,\"file\":\"image-1.jpg\",\"name\":\"image-2\",\"comment\":null,\"date_creation\":\"2013-09-07 21:40:23\",\"date_available\":\"2021-02-16 23:11:42\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\",\"element_url\":\"https:\\/\\/example.piwigo.com\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e.jpg\",\"derivatives\":{\"square\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sq.jpg\",\"width\":120,\"height\":120},\"thumb\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-th.jpg\",\"width\":96,\"height\":144},\"2small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-2s.jpg\",\"width\":160,\"height\":240},\"xsmall\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xs.jpg\",\"width\":216,\"height\":324},\"small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sm.jpg\",\"width\":288,\"height\":432},\"medium\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-me.jpg\",\"width\":396,\"height\":594},\"large\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-la.jpg\",\"width\":504,\"height\":756},\"xlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xl.jpg\",\"width\":612,\"height\":918},\"xxlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xx.jpg\",\"width\":828,\"height\":1242}},\"tags\":[{\"id\":12,\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/tags\\/12-needs_tags\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\\/tags\\/12-needs_tags\"}]}]}}",
        headers: [],
        status: 200
      }
    }
  end

  def images_request("good_host", ["tag_name", "tag_name2"], :and, _) do
    {
      :ok,
      %Finch.Response{
        body:
          "{\"stat\":\"ok\",\"result\":{\"paging\":{\"page\":1,\"per_page\":100,\"count\":100,\"total_count\":206},\"images\":[{\"rank\":0,\"id\":121,\"width\":4912,\"height\":7360,\"hit\":0,\"file\":\"image-121.jpg\",\"name\":\"image-121\",\"comment\":null,\"date_creation\":\"2013-09-07 21:40:23\",\"date_available\":\"2021-02-16 23:11:42\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\",\"element_url\":\"https:\\/\\/example.piwigo.com\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e.jpg\",\"derivatives\":{\"square\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sq.jpg\",\"width\":120,\"height\":120},\"thumb\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-th.jpg\",\"width\":96,\"height\":144},\"2small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-2s.jpg\",\"width\":160,\"height\":240},\"xsmall\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xs.jpg\",\"width\":216,\"height\":324},\"small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sm.jpg\",\"width\":288,\"height\":432},\"medium\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-me.jpg\",\"width\":396,\"height\":594},\"large\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-la.jpg\",\"width\":504,\"height\":756},\"xlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xl.jpg\",\"width\":612,\"height\":918},\"xxlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xx.jpg\",\"width\":828,\"height\":1242}},\"tags\":[{\"id\":12,\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/tags\\/12-needs_tags\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\\/tags\\/12-needs_tags\"}]}]}}",
        headers: [],
        status: 200
      }
    }
  end

  def images_request("good_host", ["tag_name", "tag_name2"], :or, _) do
    {
      :ok,
      %Finch.Response{
        body:
          "{\"stat\":\"ok\",\"result\":{\"paging\":{\"page\":1,\"per_page\":100,\"count\":100,\"total_count\":206},\"images\":[{\"rank\":0,\"id\":204,\"width\":4912,\"height\":7360,\"hit\":0,\"file\":\"image-204.jpg\",\"name\":\"image-204\",\"comment\":null,\"date_creation\":\"2013-09-07 21:40:23\",\"date_available\":\"2021-02-16 23:11:42\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\",\"element_url\":\"https:\\/\\/example.piwigo.com\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e.jpg\",\"derivatives\":{\"square\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sq.jpg\",\"width\":120,\"height\":120},\"thumb\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-th.jpg\",\"width\":96,\"height\":144},\"2small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-2s.jpg\",\"width\":160,\"height\":240},\"xsmall\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xs.jpg\",\"width\":216,\"height\":324},\"small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sm.jpg\",\"width\":288,\"height\":432},\"medium\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-me.jpg\",\"width\":396,\"height\":594},\"large\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-la.jpg\",\"width\":504,\"height\":756},\"xlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xl.jpg\",\"width\":612,\"height\":918},\"xxlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xx.jpg\",\"width\":828,\"height\":1242}},\"tags\":[{\"id\":12,\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/tags\\/12-needs_tags\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\\/tags\\/12-needs_tags\"}]}]}}",
        headers: [],
        status: 200
      }
    }
  end

  def images_request("good_host", _, _, _) do
    {
      :ok,
      %Finch.Response{
        body:
          "{\"stat\":\"ok\",\"result\":{\"paging\":{\"page\":0,\"per_page\":100,\"count\":100,\"total_count\":206},\"images\":[{\"rank\":0,\"id\":366,\"width\":4912,\"height\":7360,\"hit\":0,\"file\":\"image-1.jpg\",\"name\":\"image-1\",\"comment\":null,\"date_creation\":\"2013-09-07 21:40:23\",\"date_available\":\"2021-02-16 23:11:42\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\",\"element_url\":\"https:\\/\\/example.piwigo.com\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e.jpg\",\"derivatives\":{\"square\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sq.jpg\",\"width\":120,\"height\":120},\"thumb\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-th.jpg\",\"width\":96,\"height\":144},\"2small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-2s.jpg\",\"width\":160,\"height\":240},\"xsmall\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xs.jpg\",\"width\":216,\"height\":324},\"small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-sm.jpg\",\"width\":288,\"height\":432},\"medium\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-me.jpg\",\"width\":396,\"height\":594},\"large\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-la.jpg\",\"width\":504,\"height\":756},\"xlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xl.jpg\",\"width\":612,\"height\":918},\"xxlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216231142-1b4db16e-xx.jpg\",\"width\":828,\"height\":1242}},\"tags\":[{\"id\":12,\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/tags\\/12-needs_tags\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/366\\/tags\\/12-needs_tags\"}]}]}}",
        headers: [],
        status: 200
      }
    }
  end
end
