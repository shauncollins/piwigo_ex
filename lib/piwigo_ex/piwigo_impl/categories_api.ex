defmodule PiwigoEx.PiwigoImpl.CategoriesAPI do
  @behaviour PiwigoEx.PiwigoImpl.CategoriesInterface
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint

  @moduledoc """
  Makes a category requests to Piwigo server
  """

  @doc """
  Get the list of categories on the server
  """
  def category_request(host) do
    CallEndpoint.get("#{Config.url(host)}&method=pwg.categories.getList")
  end
end
