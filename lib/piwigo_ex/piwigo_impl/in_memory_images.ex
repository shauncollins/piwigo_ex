defmodule PiwigoEx.PiwigoImpl.InMemoryImages do
  @moduledoc false

  def set_info_request("bad_host", _, _, _, _) do
    {
      :ok,
      %Finch.Response{
        status: 404
      }
    }
  end

  def set_info_request("good_host", "bad_token", _, _, _) do
    {
      :ok,
      %Finch.Response{
        body: "{\"stat\":\"fail\",\"err\":401,\"message\":\"Access denied\"}",
        status: 401
      }
    }
  end

  def set_info_request("good_host", "good_token", "bad_id", _, _) do
    {
      :ok,
      %Finch.Response{
        body: "{\"stat\":\"fail\",\"err\":404,\"message\":\"image_id not found\"}",
        status: 404
      }
    }
  end

  def set_info_request("good_host", "good_token", "image_id", _, _) do
    {
      :ok,
      %Finch.Response{
        body: "{\"stat\":\"ok\",\"result\":null}",
        status: 200
      }
    }
  end

  def get_info_request("bad_host", _) do
    {
      :ok,
      %Finch.Response{
        status: 404
      }
    }
  end

  def get_info_request("good_host", "bad_id") do
    {
      :ok,
      %Finch.Response{
        body: "{\"stat\":\"fail\",\"err\":404,\"message\":\"image_id not found\"}",
        status: 404
      }
    }
  end

  def get_info_request("good_host", _) do
    {
      :ok,
      %Finch.Response{
        body:
          "{\"stat\":\"ok\",\"result\":{\"id\":41,\"file\":\"IMG_5403.JPG\",\"date_available\":\"2021-02-18 00:51:46\",\"date_creation\":\"2021-02-17 18:48:20\",\"name\":\"IMG_5403.JPG\",\"comment\":null,\"author\":null,\"hit\":5,\"filesize\":263,\"width\":1174,\"height\":880,\"coi\":null,\"representative_ext\":null,\"date_metadata_update\":\"2021-02-18\",\"rating_score\":null,\"level\":\"0\",\"md5sum\":\"d84d68209d3d73c223ce0312ab97a791\",\"added_by\":\"1\",\"rotation\":\"0\",\"latitude\":null,\"longitude\":null,\"lastmodified\":\"2021-03-19 01:41:33\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/41\",\"element_url\":\"https:\\/\\/example.piwigo.com\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820.jpg\",\"derivatives\":{\"square\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820-sq.jpg\",\"width\":120,\"height\":120},\"thumb\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820-th.jpg\",\"width\":144,\"height\":107},\"2small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820-2s.jpg\",\"width\":240,\"height\":179},\"xsmall\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820-xs.jpg\",\"width\":432,\"height\":323},\"small\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820-sm.jpg\",\"width\":576,\"height\":431},\"medium\":{\"url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820-me.jpg\",\"width\":792,\"height\":593},\"large\":{\"url\":\"https:\\/\\/example.piwigo.com\\/i?\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820-la.jpg\",\"width\":1008,\"height\":755},\"xlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820.jpg\",\"width\":\"1174\",\"height\":\"880\"},\"xxlarge\":{\"url\":\"https:\\/\\/example.piwigo.com\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/18\\/20210218005146-d84d6820.jpg\",\"width\":\"1174\",\"height\":\"880\"}},\"rates\":{\"score\":null,\"count\":0,\"average\":null},\"categories\":[{\"id\":1,\"name\":\"Dad\",\"permalink\":null,\"uppercats\":\"1\",\"global_rank\":\"2\",\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/category\\/1-dad\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/41\\/category\\/1-dad\"}],\"tags\":[{\"id\":12,\"name\":\"Needs Tags\",\"url_name\":\"needs_tags\",\"lastmodified\":\"2021-02-28 17:09:12\",\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/tags\\/12-needs_tags\",\"page_url\":\"https:\\/\\/example.piwigo.com\\/picture?\\/377\\/tags\\/12-needs_tags\"}],\"comments_paging\":{\"page\":0,\"per_page\":\"10\",\"count\":0,\"total_count\":0},\"comments\":[]}}",
        headers: [],
        status: 200
      }
    }
  end
end
