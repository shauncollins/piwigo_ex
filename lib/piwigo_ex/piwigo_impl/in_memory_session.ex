defmodule PiwigoEx.PiwigoImpl.InMemorySession do
  @behaviour PiwigoEx.PiwigoImpl.SessionInterface

  @moduledoc """
  Fakes session requests to Piwigo server to simulate logging in and out
  """

  def login_request(_, _, "bad_host") do
    {:ok,
     %Finch.Response{
       status: 404
     }}
  end

  def login_request("username", "wrong_pass", "good_host") do
    {:ok,
     %Finch.Response{
       body: "{\"stat\":\"fail\",\"err\":999,\"message\":\"Invalid username\\/password\"}",
       headers: [],
       status: 200
     }}
  end

  def login_request("username", "right_pass", "good_host") do
    {:ok,
     %Finch.Response{
       body: "{\"stat\":\"ok\",\"result\":true}",
       headers: [
         {"set-cookie", "pwg_id=token; path=/; HttpOnly"}
       ],
       status: 200
     }}
  end

  def logout_request("bad_host") do
    {:ok,
     %Finch.Response{
       status: 404
     }}
  end

  def logout_request("good_host") do
    {:ok,
     %Finch.Response{
       body: "{\"stat\":\"ok\",\"result\":true}",
       headers: [],
       status: 200
     }}
  end
end
