defmodule PiwigoEx.PiwigoImpl.TagsAPI do
  @behaviour PiwigoEx.PiwigoImpl.TagsInterface
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint

  @moduledoc """
  Makes tag requests to the Piwigo server.
  """

  @doc """
  Get the list of tags on the server
  """
  def tags_request(host) do
    CallEndpoint.get("#{Config.url(host)}&method=pwg.tags.getList")
  end

  @doc """
  Get the list of tags on the server
  """
  def admin_tags_request(host, token) do
    CallEndpoint.get(
      "#{Config.url(host)}&method=pwg.tags.getAdminList",
      [{"Cookie", token}]
    )
  end

  @doc """
  Get a list of images by tag name
  """
  def images_request(
        host,
        tag_names,
        and_slash_or,
        %PiwigoEx.PageRequest{
          page: page,
          per_page: per_page
        }
      )
      when is_binary(host) and is_list(tag_names) and is_atom(and_slash_or) do
    safe_tag_names =
      tag_names
      |> Enum.map(fn x -> "tag_name[]=#{x}" end)
      |> Enum.join("&")

    tag_mode_and =
      case and_slash_or do
        :and -> "true"
        :or -> "false"
      end

    CallEndpoint.post(
      host,
      "method=pwg.tags.getImages&#{safe_tag_names}&tag_mode_and=#{tag_mode_and}&per_page=#{
        per_page
      }&page=#{page}"
    )
  end
end
