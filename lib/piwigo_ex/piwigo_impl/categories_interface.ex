defmodule PiwigoEx.PiwigoImpl.CategoriesInterface do
  @moduledoc """
  A list of all the category requests made to the server
  """

  @callback category_request(host :: String.t()) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}
end
