defmodule PiwigoEx.PiwigoImpl.SessionAPI do
  @behaviour PiwigoEx.PiwigoImpl.SessionInterface
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint

  @moduledoc """
  Makes session requests to the Piwigo server for logging in and getting tokens and logging out
  """

  @doc """
  Attempt logging in to the server with the given username and password
  """
  def login_request(username, password, host) do
    CallEndpoint.post(host, "method=pwg.session.login&username=#{username}&password=#{password}")
  end

  @doc """
  Log out of the server
  """
  def logout_request(host) do
    CallEndpoint.get("#{Config.url(host)}&method=pwg.session.logout")
  end
end
