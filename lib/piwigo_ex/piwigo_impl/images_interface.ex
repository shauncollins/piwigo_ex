defmodule PiwigoEx.PiwigoImpl.ImagesInterface do
  @moduledoc """
  A list of all the images requests made to the server
  """

  @callback set_info_request(
              host :: String.t(),
              token :: String.t(),
              image_id :: Integer.t(),
              tag_names :: [String.t()],
              multiple_value_mode :: :append | :replace
            ) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}

  @callback get_info_request(
              host :: String.t(),
              image_id :: Integer.t()
            ) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}
end
