defmodule PiwigoEx.PiwigoImpl.SessionInterface do
  @moduledoc """
  A list of all the session requests made to the server
  """

  @callback login_request(
              username :: String.t(),
              password :: String.t(),
              host :: String.t()
            ) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}

  @callback logout_request(host :: String.t()) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}
end
