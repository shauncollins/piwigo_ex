defmodule PiwigoEx.PiwigoImpl.InMemoryCategories do
  @behaviour PiwigoEx.PiwigoImpl.CategoriesInterface

  @moduledoc """
  Fakes category requests to Piwigo server
  """

  def category_request("bad_host") do
    {:ok,
     %Finch.Response{
       status: 404
     }}
  end

  def category_request("one_category") do
    {:ok,
     %Finch.Response{
       body:
         "{\"stat\":\"ok\",\"result\":{\"categories\":[{\"id\":1,\"name\":\"one\",\"comment\":\"\",\"permalink\":null,\"status\":\"public\",\"uppercats\":\"1\",\"global_rank\":\"2\",\"id_uppercat\":null,\"nb_images\":206,\"total_nb_images\":206,\"representative_picture_id\":\"1\",\"date_last\":\"2021-02-16 23:11:42\",\"max_date_last\":\"2021-02-16 23:11:42\",\"nb_categories\":0,\"url\":\"https:\\/\\/example.piwigo.com\\/index?\\/category\\/1-one\",\"tn_url\":\"https:\\/\\/example.piwigo.com\\/_datas\\/s\\/y\\/d\\/sydlu8vfje\\/i\\/uploads\\/s\\/y\\/d\\/sydlu8vfje\\/\\/2021\\/02\\/16\\/20210216211725-d08c0c0c-th.jpg\"}]}}",
       headers: [],
       status: 200
     }}
  end
end
