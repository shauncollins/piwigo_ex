defmodule PiwigoEx.PiwigoImpl.TagsInterface do
  @moduledoc """
  A list of all the tags requests made to the server
  """

  @callback tags_request(host :: String.t()) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}

  @callback images_request(
              host :: String.t(),
              tag_names :: [String.t()],
              and_slash_or :: :and | :or,
              page_request :: PiwigoEx.PageRequest.t()
            ) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}

  @callback admin_tags_request(
              host :: String.t(),
              token :: String.t()
            ) ::
              {:ok, Response.t()}
              | {:error, Mint.Types.error()}
end
