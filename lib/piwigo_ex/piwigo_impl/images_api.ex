defmodule PiwigoEx.PiwigoImpl.ImagesAPI do
  @behaviour PiwigoEx.PiwigoImpl.ImagesInterface
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint

  @moduledoc """
  Make images requests to Piwigo server
  """

  @doc """
  Update the info in an image (currently just the tags)
  """
  def set_info_request(host, token, image_id, tag_ids, multiple_value_mode) do
    tag_ids_string = Enum.join(tag_ids, ",")

    multiple_value_mode_string =
      case multiple_value_mode do
        :replace -> "replace"
        :append -> "append"
      end

    CallEndpoint.post(
      host,
      "method=pwg.images.setInfo&image_id=#{image_id}&tag_ids=#{tag_ids_string}&multiple_value_mode=#{
        multiple_value_mode_string
      }",
      [{"Cookie", token}]
    )
  end

  def get_info_request(host, image_id) do
    CallEndpoint.get("#{Config.url(host)}&method=pwg.images.getInfo&image_id=#{image_id}")
  end
end
