defmodule PiwigoEx.Paging do
  @moduledoc """
  Helper methods for dealing with paged responses from PiwigoEx
  """

  @doc """
  Return true if the paging response indicates there are more pages of images
  """
  def has_more_pages(%{
        "page" => page,
        "per_page" => per_page,
        "count" => count,
        "total_count" => total_count
      })
      when is_integer(page) and is_integer(per_page) and is_integer(count) and
             is_integer(total_count) do
    page * per_page + count < total_count
  end

  @doc """
  Given the current paging response, create a page request to get the next page of data.
  """
  def next_page(%{"page" => page, "per_page" => per_page}) do
    %PiwigoEx.PageRequest{page: page + 1, per_page: per_page}
  end
end
