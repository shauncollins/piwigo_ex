defmodule PiwigoEx.PageRequest do
  @moduledoc """
  A common structure to request paged results
  """
  defstruct page: 0, per_page: 100
end
