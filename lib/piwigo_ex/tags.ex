defmodule PiwigoEx.Tags do
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint
  alias PiwigoEx.Helpers.ErrorHandler
  @moduledoc false

  @doc """
  Get the list of tags from the Piwigo host.  Currently the only option to pass in is the host: [host: "host"]
  """
  def get_tags_list(opts \\ []) do
    host = Keyword.get(opts, :host, Config.host())

    function = fn -> Config.tags_api().tags_request(host) end

    case CallEndpoint.handle_result(function) do
      {:success, json} ->
        Map.fetch!(json, "result")
        |> Map.fetch("tags")

      err ->
        ErrorHandler.handle_error(err)
    end
  end

  @doc """
  Get the list of admin tags from the Piwigo host using the given token.  Currently the only option to pass in is the
  host: [host: "host"]
  """
  def get_admin_tags_list(token, opts \\ []) do
    host = Keyword.get(opts, :host, Config.host())

    function = fn -> Config.tags_api().admin_tags_request(host, token) end

    case CallEndpoint.handle_result(function) do
      {:success, json} ->
        Map.fetch!(json, "result")
        |> Map.fetch("tags")

      err ->
        ErrorHandler.handle_error(err)
    end
  end

  @doc """
  Get the list of images for a tag name or multiple tag names if you use the array version.  Options are:
  host: "host"
  page_request: a PiwigoEx.PageRequest with a page number and images per page listed
  and_slash_or with either :and or :or.  :and requires all tags in the array to be on the image, :or will find an image
  that has any of the given tags.
  """
  def get_images(tag_name, opts \\ [])

  def get_images(tag_name, opts) when is_binary(tag_name) do
    get_images([tag_name], opts)
  end

  def get_images(tag_names, opts) when is_list(tag_names) do
    and_slash_or = Keyword.get(opts, :and_slash_or, :or)
    host = Keyword.get(opts, :host, Config.host())
    page_request = Keyword.get(opts, :page_request, %PiwigoEx.PageRequest{})

    function = fn ->
      Config.tags_api().images_request(host, tag_names, and_slash_or, page_request)
    end

    case CallEndpoint.handle_result(function) do
      {:success, json} ->
        #  Get the list of images and paging data
        Map.fetch(json, "result")

      err ->
        ErrorHandler.handle_error(err)
    end
  end
end
