defmodule PiwigoEx.Images do
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint
  alias PiwigoEx.Helpers.ErrorHandler

  @moduledoc """
  Interact with a subset of the Piwigo.Images endpoints
  """

  def update_tags(image_id, token, tag_ids, opts \\ []) do
    host = Keyword.get(opts, :host, Config.host())
    multiple_value_mode = Keyword.get(opts, :multiple_value_mode, :append)

    function = fn ->
      Config.images_api().set_info_request(
        host,
        token,
        image_id,
        tag_ids,
        multiple_value_mode
      )
    end

    case CallEndpoint.handle_result(function) do
      {:success, _json} -> :ok
      err -> ErrorHandler.handle_error(err)
    end
  end

  def get_image_info(image_id, opts \\ []) do
    host = Keyword.get(opts, :host, Config.host())

    function = fn ->
      Config.images_api().get_info_request(
        host,
        image_id
      )
    end

    case CallEndpoint.handle_result(function) do
      {:success, json} -> Map.fetch(json, "result")
      err -> ErrorHandler.handle_error(err)
    end
  end
end
