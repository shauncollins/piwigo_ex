defmodule PiwigoEx.Categories do
  alias PiwigoEx.Config
  alias PiwigoEx.Helpers.CallEndpoint
  alias PiwigoEx.Helpers.ErrorHandler
  @moduledoc false

  @doc """
  Get the list of categories/albums from the Piwigo host.  Currently the only opt is [host: hostname]
  """
  def get_category_list(opts \\ []) do
    host = Keyword.get(opts, :host, Config.host())
    call = fn -> Config.categories_api().category_request(host) end

    case CallEndpoint.handle_result(call) do
      {:success, json} ->
        #  Get the list of categories from the result object in the response
        Map.fetch!(json, "result")
        |> Map.fetch("categories")

      err ->
        ErrorHandler.handle_error(err)
    end
  end
end
