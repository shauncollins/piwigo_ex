defmodule PiwigoEx.Config do
  @moduledoc false

  def session_api do
    Application.get_env(:piwigo_ex, :session_api)
  end

  def categories_api do
    Application.get_env(:piwigo_ex, :categories_api)
  end

  def tags_api do
    Application.get_env(:piwigo_ex, :tags_api)
  end

  def images_api do
    Application.get_env(:piwigo_ex, :images_api)
  end

  def url(host) do
    "#{host}/ws.php?format=json"
  end

  def host() do
    case System.fetch_env("PIWIGO_HOST") do
      {:ok, host} -> host
      _ -> nil
    end
  end
end
