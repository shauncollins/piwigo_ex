defmodule PiwigoEx.TagsTest do
  use ExUnit.Case

  alias PiwigoEx.Tags

  @moduletag :capture_log

  test "can not list public tags with an invalid host" do
    assert Tags.get_tags_list(host: "bad_host") == {:error, :invalid_url}
  end

  test "can list public tags with a good host" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, list} = Tags.get_tags_list(host: "one_tag")
    assert Enum.count(list) == 1

    name =
      List.first(list)
      |> Map.fetch!("name")

    assert name == "tags"
  end

  test "can not list admin tags with an invalid host" do
    assert Tags.get_admin_tags_list("good_token", host: "bad_host") == {:error, :invalid_url}
  end

  test "can not list admin tags with an invalid token" do
    assert Tags.get_admin_tags_list("bad_token", host: "good_host") ==
             {:error, :unauthorized}
  end

  test "can list admin tags with a good host and token" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, list} = Tags.get_admin_tags_list("good_token", host: "good_host")
    assert Enum.count(list) == 1

    name =
      List.first(list)
      |> Map.fetch!("name")

    assert name == "tags"
  end

  test "can not get images by tag with an invalid host" do
    assert Tags.get_images("tag_name", host: "bad_host") == {:error, :invalid_url}
  end

  test "can get images by tag with a good host" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, result} = Tags.get_images("tag_name", host: "good_host")
    {:ok, images} = Map.fetch(result, "images")
    {:ok, pages} = Map.fetch(result, "paging")

    assert Enum.count(images) == 1

    name =
      List.first(images)
      |> Map.fetch!("name")

    assert name == "image-1"
    assert Map.fetch(pages, "page") == {:ok, 0}
  end

  test "can get images by tag and paging request with a good host" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, result} =
      Tags.get_images("tag_name", host: "good_host", page_request: %PiwigoEx.PageRequest{page: 1})

    {:ok, images} = Map.fetch(result, "images")
    {:ok, pages} = Map.fetch(result, "paging")

    assert Enum.count(images) == 1

    name =
      List.first(images)
      |> Map.fetch!("name")

    assert name == "image-2"
    assert Map.fetch(pages, "page") == {:ok, 1}
  end

  test "can get images by tags (using and) with a good host" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, result} =
      Tags.get_images(["tag_name", "tag_name2"], host: "good_host", and_slash_or: :and)

    {:ok, images} = Map.fetch(result, "images")

    assert Enum.count(images) == 1

    name =
      List.first(images)
      |> Map.fetch!("name")

    assert name == "image-121"
  end

  test "can get images by tags (using or) with a good host" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, result} =
      Tags.get_images(["tag_name", "tag_name2"], host: "good_host", and_slash_or: :or)

    {:ok, images} = Map.fetch(result, "images")

    assert Enum.count(images) == 1

    name =
      List.first(images)
      |> Map.fetch!("name")

    assert name == "image-204"
  end

  test "when getting images by tags the default is or" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, result} = Tags.get_images(["tag_name", "tag_name2"], host: "good_host")
    {:ok, images} = Map.fetch(result, "images")

    assert Enum.count(images) == 1

    name =
      List.first(images)
      |> Map.fetch!("name")

    assert name == "image-204"
  end
end
