defmodule PiwigoEx.CategoriesTest do
  use ExUnit.Case

  alias PiwigoEx.Categories

  @moduletag :capture_log

  test "can not list categories with an invalid host" do
    assert Categories.get_category_list(host: "bad_host") == {:error, :invalid_url}
  end

  test "can list categories with a good host" do
    # Right now we only care that there is one item and it has the name "one"
    {:ok, list} = Categories.get_category_list(host: "one_category")
    assert Enum.count(list) == 1

    name =
      List.first(list)
      |> Map.fetch!("name")

    assert name == "one"
  end
end
