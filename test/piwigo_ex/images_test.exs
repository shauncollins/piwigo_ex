defmodule PiwigoEx.ImagesTest do
  use ExUnit.Case

  alias PiwigoEx.Images

  @moduletag :capture_log

  test "can not add tags with an invalid host" do
    assert Images.update_tags("image_id", "good_token", [1, 2], host: "bad_host") ==
             {:error, :invalid_url}
  end

  test "can not add tags with an invalid token" do
    assert Images.update_tags("image_id", "bad_token", [1, 2], host: "good_host") ==
             {:error, :unauthorized}
  end

  test "can not add tags to an invalid image id" do
    assert Images.update_tags("bad_id", "good_token", [1, 2], host: "good_host") ==
             {:error, :invalid_image_id}
  end

  test "can add tags to a valid image id" do
    assert Images.update_tags("image_id", "good_token", [1, 2], host: "good_host") == :ok
  end

  test "can not get an image with an invalid host" do
    assert Images.get_image_info("hello", host: "bad_host") == {:error, :invalid_url}
  end

  test "can not get an image that doesn't exist" do
    assert Images.get_image_info("bad_id", host: "good_host") == {:error, :invalid_image_id}
  end

  test "can get a valid image" do
    {:ok, image} = Images.get_image_info(200, host: "good_host")
    {:ok, id} = Map.fetch(image, "id")
    assert id == 41
  end
end
