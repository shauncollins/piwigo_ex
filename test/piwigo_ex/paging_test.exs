defmodule PiwigoEx.PagingTest do
  use ExUnit.Case

  alias PiwigoEx.Paging

  @moduletag :capture_log

  doctest Paging

  describe "Paging.has_more_pages" do
    test "returns true when there are more pages" do
      assert Paging.has_more_pages(%{
               "page" => 0,
               "per_page" => 10,
               "count" => 10,
               "total_count" => 20
             }) == true
    end

    test "returns false when on the last page" do
      assert Paging.has_more_pages(%{
               "page" => 1,
               "per_page" => 10,
               "count" => 10,
               "total_count" => 20
             }) == false
    end

    test "returns true when it should be the last page but count is wrong" do
      assert Paging.has_more_pages(%{
               "page" => 1,
               "per_page" => 10,
               "count" => 7,
               "total_count" => 20
             }) == true
    end

    test "returns false when past the last page" do
      assert Paging.has_more_pages(%{
               "page" => 4,
               "per_page" => 10,
               "count" => 10,
               "total_count" => 20
             }) == false
    end
  end

  describe "Paging.next_page" do
    test "returns page 1 when on the first page" do
      assert Paging.next_page(%{"page" => 0, "per_page" => 10}) == %PiwigoEx.PageRequest{
               page: 1,
               per_page: 10
             }
    end

    test "returns page 2 when on the 2nd page" do
      assert Paging.next_page(%{"page" => 1, "per_page" => 10}) == %PiwigoEx.PageRequest{
               page: 2,
               per_page: 10
             }
    end
  end
end
