defmodule PiwigoEx.SessionTest do
  use ExUnit.Case

  alias PiwigoEx.Session

  @moduletag :capture_log

  test "can not log in on an invalid host" do
    assert Session.login(user: "username", pass: "password", host: "bad_host") ==
             {:error, :invalid_url}
  end

  test "can not log in on a bad username/password" do
    assert Session.login(user: "username", pass: "wrong_pass", host: "good_host") ==
             {:error, :invalid_username_password}
  end

  test "can log in on a good username/password" do
    assert Session.login(user: "username", pass: "right_pass", host: "good_host") ==
             {:ok, "pwg_id=token"}
  end

  describe "single site instance" do
    setup do
      real_host =
        case System.fetch_env("PIWIGO_HOST") do
          {:ok, host} ->
            # Remove the env variable for the test
            :ok = System.delete_env("PIWIGO_HOST")
            host

          :error ->
            nil
        end

      on_exit(fn -> reset_host(real_host) end)
    end

    test "can not log in on a single site instance without a site environment variable" do
      assert_raise FunctionClauseError, fn ->
        Session.login(user: "username", pass: "right_pass")
      end
    end

    test "can not log in on an single site instance with an invalid host" do
      :ok = System.put_env("PIWIGO_HOST", "bad_host")
      assert Session.login(user: "username", pass: "password") == {:error, :invalid_url}
    end

    test "can not log in on a site site instance with a bad password" do
      :ok = System.put_env("PIWIGO_HOST", "good_host")

      assert Session.login(user: "username", pass: "wrong_pass") ==
               {:error, :invalid_username_password}
    end

    test "can log in on a site site instance with a good username/password" do
      :ok = System.put_env("PIWIGO_HOST", "good_host")
      assert Session.login(user: "username", pass: "right_pass") == {:ok, "pwg_id=token"}
    end
  end

  describe "Single Site/Single User" do
    setup do
      real_host =
        case System.fetch_env("PIWIGO_HOST") do
          {:ok, host} ->
            # Remove the env variable for the test
            :ok = System.delete_env("PIWIGO_HOST")
            host

          :error ->
            nil
        end

      real_user =
        case System.fetch_env("PIWIGO_USER") do
          {:ok, user} ->
            # Remove the env variable for the test
            :ok = System.delete_env("PIWIGO_USER")
            user

          :error ->
            nil
        end

      real_pass =
        case System.fetch_env("PIWIGO_PASS") do
          {:ok, pass} ->
            # Remove the env variable for the test
            :ok = System.delete_env("PIWIGO_PASS")
            pass

          :error ->
            nil
        end

      on_exit(fn ->
        reset_host(real_host)
        reset_user(real_user)
        reset_pass(real_pass)
      end)
    end

    test "can not log in on a site site single user instance without a site environment variable" do
      :ok = System.put_env("PIWIGO_USER", "username")
      :ok = System.put_env("PIWIGO_PASS", "right_pass")
      assert_raise FunctionClauseError, fn -> Session.login() end
    end

    test "can not log in on a site site single user instance without a username environment variable" do
      :ok = System.put_env("PIWIGO_HOST", "good_host")
      :ok = System.put_env("PIWIGO_PASS", "right_pass")
      assert_raise FunctionClauseError, fn -> Session.login() end
    end

    test "can not log in on a site site single user instance without a pass environment variable" do
      :ok = System.put_env("PIWIGO_HOST", "good_host")
      :ok = System.put_env("PIWIGO_USER", "username")
      assert_raise FunctionClauseError, fn -> Session.login() end
    end

    test "can not log in on an single site single user instance with an invalid host" do
      :ok = System.put_env("PIWIGO_HOST", "bad_host")
      :ok = System.put_env("PIWIGO_USER", "username")
      :ok = System.put_env("PIWIGO_PASS", "wrong_pass")
      assert Session.login() == {:error, :invalid_url}
    end

    test "can not log in on a site site instance with a bad password" do
      :ok = System.put_env("PIWIGO_HOST", "good_host")
      :ok = System.put_env("PIWIGO_USER", "username")
      :ok = System.put_env("PIWIGO_PASS", "wrong_pass")
      assert Session.login() == {:error, :invalid_username_password}
    end

    test "can log in on a site site instance with a good username/password" do
      :ok = System.put_env("PIWIGO_HOST", "good_host")
      :ok = System.put_env("PIWIGO_USER", "username")
      :ok = System.put_env("PIWIGO_PASS", "right_pass")
      assert Session.login(user: "username", pass: "right_pass") == {:ok, "pwg_id=token"}
    end
  end

  test "can not log out with an invalid host" do
    assert Session.logout(host: "bad_host") == {:error, :invalid_url}
  end

  test "can log out with an good host" do
    assert Session.logout(host: "good_host") == :ok
  end

  defp reset_host(nil) do
    System.delete_env("PIWIGO_HOST")
  end

  defp reset_host(host) do
    System.put_env("PIWIGO_HOST", host)
  end

  defp reset_user(nil) do
    System.delete_env("PIWIGO_USER")
  end

  defp reset_user(user) do
    System.put_env("PIWIGO_USER", user)
  end

  defp reset_pass(nil) do
    System.delete_env("PIWIGO_PASS")
  end

  defp reset_pass(pass) do
    System.put_env("PIWIGO_PASS", pass)
  end
end
