# PiwigoEx

A library for interacting with a small subset of functionality that Piwigo offers.

## Current Features
 * Login
   
`PiwigoEx.Session.login([host: "https://example.piwigo.com", user: "User", pass: "Password"])`
 * Logout
   
`PiwigoEx.Session.logout([host: "https://example.piwigo.com"])`
 * Get the list of categories
   
`PiwigoEx.Categories.get_category_list([host: "https://example.piwigo.com"])`
 * Get the list of publicly available tags
   
`PiwigoEx.Tags.get_tags_list([host: "https://example.piwigo.com"])`
 * Get the list of all tags **

`PiwigoEx.Tags.get_admin_tags_list("token", [host: "https://example.piwigo.com"])`
 * Get all the images for a tag with paging
   
`PiwigoEx.Tags.get_images("tag_name", [host: "https://example.piwigo.com", page_request: %PiwigoEx.PageRequest{page: 4}])`
 * Get all the images in a list of tags (using and/or to combine)

`PiwigoEx.Tags.get_images(["tag_name", "tag_2"], [host: "https://example.piwigo.com", and_slash_or: :or, page_request: %PiwigoEx.PageRequest{page: 4}])`

 * Get the image info

`PiwigoEx.Images.get_image_info(200, [host: "https://example.piwigo.com"])`

 * Update tags on an image **

`PiwigoEx.Images.update_tags(200, "token", [14, 9], [host: "https://example.piwigo.com"])`

** - Authorized users only

If the server will always interact with 1 Piwigo server, the following environment variables can be read instead of passing host, user, pass around:
PIWIGO_USER

PIWIGO_PASS

PIWIGO_HOST

For example:

`PiwigoEx.Tags.get_images(["tag_name", "tag_2"], [and_slash_or: :or, page_request: %PiwigoEx.PageRequest{page: 4}])`

