use Mix.Config

config :piwigo_ex, :session_api, PiwigoEx.PiwigoImpl.SessionAPI
config :piwigo_ex, :categories_api, PiwigoEx.PiwigoImpl.CategoriesAPI
config :piwigo_ex, :tags_api, PiwigoEx.PiwigoImpl.TagsAPI
config :piwigo_ex, :images_api, PiwigoEx.PiwigoImpl.ImagesAPI
