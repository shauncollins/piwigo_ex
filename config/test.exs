use Mix.Config

config :piwigo_ex, :session_api, PiwigoEx.PiwigoImpl.InMemorySession
config :piwigo_ex, :categories_api, PiwigoEx.PiwigoImpl.InMemoryCategories
config :piwigo_ex, :tags_api, PiwigoEx.PiwigoImpl.InMemoryTags
config :piwigo_ex, :images_api, PiwigoEx.PiwigoImpl.InMemoryImages
